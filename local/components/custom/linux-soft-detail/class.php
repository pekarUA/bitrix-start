<?php

use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}

class DetailLinuxSoft extends CBitrixComponent
{
    /**
     * @throws LoaderExceptionv
     */
    public function executeComponent()
    {
        Loader::includeModule('iblock');
        
        $arFilter = ["IBLOCK_ID" => 3, "CODE" => $this->arParams["ELEMENT_CODE"]];
        $arSelect = ["ID", "IBLOCK_ID", "CODE", "IBLOCK_NAME", "NAME", "DETAIL_TEXT", "DETAIL_PICTURE", "PROPERTY_*"];
        $res      = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
    
        $this->arResult['SOFT'] = [];
    
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $src = CFile::GetFileArray($arFields['PROPERTY_2']);

            array_push($this->arResult['SOFT'], array_merge($arFields, $src));
        }
        
        $this->includeComponentTemplate();
    }
}

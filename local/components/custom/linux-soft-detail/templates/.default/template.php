<?php

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

?>

<div>
    <?php foreach ($arResult["SOFT"] as $soft): ?>
    <div>
    <h2 align="center"><?php echo $soft["IBLOCK_NAME"];?></h2>
        <h3 align="center"><?php echo $soft["NAME"];?></h3>
    </div>
        <figure style="text-align: center;margin-bottom: 5px">
            <img src="<?php echo $soft['SRC'];?>"border="0" width="200px" height="200px">
            <figcaption><?php echo $soft['DETAIL_TEXT'];?></figcaption>
        </figure>
    <?php
        $version = '';
        foreach ($soft['PROPERTY_3'] as $vers) {
            $version .= $vers . ',' ;
        }
        ?>
        <p align="center"><b>Доступно на ОС:</b> <?php echo $soft['PROPERTY_4'];?><br>
            <b>Последние версии продукта:</b> <?php echo substr($version, 0, -1);?>
        </p>
    <?php endforeach;?>
</div>




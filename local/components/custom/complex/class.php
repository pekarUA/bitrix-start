<?php

use Bitrix\Main\Loader;
use \Bitrix\Iblock\Component\Tools;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}

class ComplexLinuxSoft extends CBitrixComponent
{
    public function executeComponent()
    {
        $componentPage = $this->setPageComponent();
        $this->includeComponentTemplate($componentPage);
    }
    
    /**
     * @return string
     *
     * @throws \Bitrix\Main\LoaderException
     */
    private function setPageComponent() : string
    {
        $arUrlTemplates = [
            "list" => "custom:linux-soft",
            "detail" => "#ELEMENT_CODE#/"
        ];
        $arVaribles = [];
    
        $engine = new CComponentEngine($this);
        $componentPage = $engine->ParseComponentPath(
            $this->arParams["SEF_FOLDER"],
            $arUrlTemplates,
            $arVaribles
        );
    
        $this->arResult['SEF_VAR'] = $arVaribles;
        
        Loader::includeModule('iblock');
        
        if (empty($componentPage)) {
            Tools::process404("" ,true ,true ,true ,"");
        } else {
            $error404 = false;
            
            if (isset($this->arResult['SEF_VARIABLES']['ELEMENT_CODE'])) {
                $arFilter = ["IBLOCK_ID"=> 3,  "CODE" => $this->arResult['SEF_VAR']['ELEMENT_CODE']];
                $res = CIBlockElement::GetList([], $arFilter, false, false, ['*']);

                if (!$res->GetNext()) {
                    $error404 = true;
                }
            }
        }
    
        if ($error404) {
            Tools::process404("" ,true ,true ,true ,"");
        }
        
        return $componentPage;
    }
}

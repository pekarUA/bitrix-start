<?php

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("linux-soft");
?>

<?php $APPLICATION->IncludeComponent(
    'custom:complex',
    '.default',
    [
        "COMPONENT_TEMPLATE" => ".default",
        "ID_INFOBLOCK" => "3",
        "SEF_FOLDER" => '/linux-soft/',
        "SEF_MODE" => 'Y',
    ],
    false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

<?php

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
?>

<div>
    <ul>
        <?php foreach ($arResult['SOFT'] as $item):?>
            <li content="">
                <a href="<?php echo $item['code'];?>/">
                    <h3><?php echo $item['name'];?></h3>
                </a><img src="<?php echo $item['src'];?>" border="0" width="200px" height="200px">
            </li>
        <p><?php echo $item['detail_text'];?></p>
        <p><b>Абсолютный путь к картинке: </b><?php echo $item['logo'];?></p>
        <?php endforeach;?>
    </ul>
</div>

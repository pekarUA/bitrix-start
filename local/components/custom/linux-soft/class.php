<?php

use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Iblock\Iblock;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}

class LinuxSoft extends CBitrixComponent
{
    /**
     * @throws LoaderExceptionv
     */
    public function executeComponent()
    {
        Loader::includeModule('iblock');
        
        $this->setResult();
        $this->includeComponentTemplate();
    }
    
    private function setResult()
    {
        try {
            $elements = \Bitrix\Iblock\Elements\ElementLinuxsoftTable::query()
                ->setSelect(['LOGO_' => 'LOGO.FILE', 'LAST_VERSION_' => 'LAST_VERSION', 'AVALIBLE_OS_' => 'AVALIBLE_OS', 'PREVIEW_PICTURE', 'CODE', 'NAME', 'DETAIL_TEXT'])
                ->addSelect('LOGO', ['LOGO_' => 'LOGO.FILE'])
                ->addSelect('LAST_VERSION', ['LAST_VERSION_' => 'LAST_VERSION'])
                ->addSelect('AVALIBLE_OS', ['AVALIBLE_OS_' => 'AVALIBLE_OS'])
                ->addSelect('PREVIEW_PICTURE')
                ->addSelect('CODE')
                ->addSelect('NAME')
                ->addSelect('DETAIL_TEXT')
                ->fetchCollection();
        } catch (\Bitrix\Main\ObjectPropertyException | \Bitrix\Main\SystemException | \Bitrix\Main\ArgumentException $e) {
            echo $e->getMessage();
        }
    
        $emp = [];
        foreach ($elements as $element) {
            $emp[$element->getId()] = [
                'code' => $element->getCode(),
                'name' => $element->getName(),
                'detail_text' => $element->getDetailText(),
                'available_os' => $element->getAvalibleOs()->getValue(),
                'version' => $this->getListVersion($element->getLastVersion()->getAll()),
                'logo' => $this->getUrlPicture($element->getLogo()->getFile()),
                'src' => $this->getUrlPicture($element->getLogo()->getFile(), 'src')
            ];
        }
    
        $this->arResult['SOFT'] = $emp;
    }
    
    /**
     * @param object $file
     * @param string $params
     * @return string
     */
    private function getUrlPicture(object $file, $params = 'logo'): string
    {
        if ($params === 'logo') {
            $url = sprintf('http://%s/upload/%s/%s',
                $_SERVER['HTTP_HOST'], $file->getSubdir(), $file->getFileName());
        } else {
            $url = sprintf('/upload/%s/%s',
                $file->getSubdir(), $file->getFileName());
        }
        
        return $url;
    }
    
    /**
     * @param array $arrListProperty
     * @return array
     */
    private function getListVersion(array $arrListProperty): array
    {
        $result = [];
        
        foreach ($arrListProperty as $value) {
            $result[] = $value->getValue();
        }
        
        return $result;
    }
    
    /**
     * @param array $image
     * @return string
     */
    private function resize(array $image): string
    {
        $image_src = $_SERVER['DOCUMENT_ROOT'] . $image['SRC'];
        $tmp_image = $_SERVER['DOCUMENT_ROOT'] . '/upload/iblock/' . $image['FILE_NAME'];
        
        $resize_image = CFile::ResizeImageFile(
            $image_src,
            $tmp_image,
            Array("width" => 200, "height" => 200),
            BX_RESIZE_IMAGE_EXACT);

        if ($resize_image) {
            unlink($image_src);
            rename($tmp_image, $image_src);
        }

        return $tmp_image;
    }
}
